const APP_NAME = 'My app';
const VERSION = '0.0.0';
const TECHNOLOGIES = ['HTML', 'CSS', 'JS'];

const getAppName = () => APP_NAME;

module.exports = {
  APP_NAME,
  VERSION,
  TECHNOLOGIES,
  getAppName,
};
